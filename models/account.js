/** 
*  account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const accountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accounttype: { type: String, required: true, unique: true, default: 'savings/checking' },
  accountnumber: { type: String, required: false },
  date: { type: Date, required: true, default: Date.now() }
})

module.exports = mongoose.model('Product', ProductSchema)
