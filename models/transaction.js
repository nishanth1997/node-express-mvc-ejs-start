/** 
*  Category model
*  Describes the characteristics of each attribute in a category resource.
*
* @author YeshwanthReddy Anumula <s534747@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information

const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionID: { type: Number, required: true },
  transactionDate: { type: date, required: true, default: Date.now() },
  transactionType: { type: String, enum:['NA','Cash', 'Credit', 'Debit'], required: true, default:'NA'},
  transactionAmount: {type: Number, required:true},
  status: {type: String, enum: ['Success','Failed'],required:true, default:'Success'}
})

module.exports = mongoose.model('Transaction', TransactionSchema)